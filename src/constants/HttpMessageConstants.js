const httpMessageConstants = {
  INTERNAL_SERVER_ERROR_MESSAGE: 'Internal server error',
  WRONG_AUTHORIZATION_TYPE_MESSAGE: 'Wrong authorization type, please use `Bearer`!',
  UNDEFINE_AUTHORIZATION_MESSAGE: 'Undefine Authorization, please check your authorization or login!',
  COURSE_NOT_FOUND_MESSAGE: 'Course not found'
};

export default httpMessageConstants;
