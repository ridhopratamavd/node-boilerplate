import axios from 'axios';
import Boom from '@hapi/boom';
import { isHttpStatusOK } from '../utils/HttpStatusUtils';
import httpMessageConstants from '../constants/HttpMessageConstants';

const {
  WRONG_AUTHORIZATION_TYPE_MESSAGE,
  UNDEFINE_AUTHORIZATION_MESSAGE
} = httpMessageConstants;

// eslint-disable-next-line consistent-return
const isTokenAuthorized = async (token, res, next) => {
  const url = `${process.env.GROWTH_API}/oauth/check_token?token=${token}`;
  const { status } = await axios.post(url);
  if (isHttpStatusOK(status)) {
    return next();
  }
};

const checkToken = async (req, res, next) => {
  if (process.env.BYPASS_AUTHORIZATION === true || process.env.BYPASS_AUTHORIZATION === 'true') {
    return next();
  }

  const { authorization } = req.headers;
  if (!authorization) {
    return res.json(Boom.unauthorized(UNDEFINE_AUTHORIZATION_MESSAGE).output.payload);
  }

  try {
    const spittedAuthorization = authorization.split(' ');
    if (spittedAuthorization[0] !== 'Bearer') {
      return res.json(Boom.unauthorized(WRONG_AUTHORIZATION_TYPE_MESSAGE).output.payload);
    }
    return await isTokenAuthorized(spittedAuthorization[1], res, next);
  } catch (err) {
    return next(err);
  }
};

export default checkToken;
