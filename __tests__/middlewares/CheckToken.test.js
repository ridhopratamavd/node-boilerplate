import axios from 'axios';
import checkToken from '../../src/middlewares/CheckToken';

jest.mock('axios', () => ({
  post: jest.fn()
}));

describe('#checkToken', () => {
  afterEach(() => {
    jest.clearAllMocks();
    jest.resetAllMocks();
  });
  const mockedRes = {
    json: jest.fn()
  };
  const mockedNext = jest.fn();

  it('should call res.json with status 401'
    + ' and message `Undefine Authorization, please check your authorization or login!`'
    + ' when authorization is empty', async () => {
    const req = {
      headers: {
        authorization: undefined
      }
    };

    const expectedResponse = {
      statusCode: 401,
      error: 'Unauthorized',
      message: 'Undefine Authorization, please check your authorization or login!'
    };

    await checkToken(req, mockedRes, mockedNext);

    expect(mockedRes.json).toHaveBeenCalledWith(expectedResponse);
  });

  it('should call res.json with status 401'
    + ' and message `Wrong authorization type, please use `Bearer`!`'
    + ' when authorization type is not Bearer', async () => {
    const req = {
      headers: {
        authorization: 'Oauth2 '
      }
    };

    const expectedResponse = {
      statusCode: 401,
      error: 'Unauthorized',
      message: 'Wrong authorization type, please use `Bearer`!'
    };

    await checkToken(req, mockedRes, mockedNext);

    expect(mockedRes.json).toHaveBeenCalledWith(expectedResponse);
  });

  it('should call axios.post with growth api url from env with `/oauth/check_token?token=mockToken` '
    + 'when authorization type is Bearer and token is `mockToken`', async () => {
    const req = {
      headers: {
        authorization: 'Bearer mockToken'
      }
    };

    await checkToken(req, mockedRes, mockedNext);

    expect(axios.post).toHaveBeenCalledWith(`${process.env.GROWTH_API}/oauth/check_token?token=mockToken`);
  });

  it('should call next from middleware when authorization is successful', async () => {
    const req = {
      headers: {
        authorization: 'Bearer mockToken'
      }
    };

    axios.post.mockResolvedValue({ status: 200 });
    await checkToken(req, mockedRes, mockedNext);

    expect(mockedNext).toHaveBeenCalled();
  });

  it('should call next with error parameter from middleware '
    + 'when authorization is unsuccessful', async () => {
    const req = {
      headers: {
        authorization: 'Bearer mockToken'
      }
    };

    const rejectValue = { status: 401 };
    axios.post.mockRejectedValue(rejectValue);
    await checkToken(req, mockedRes, mockedNext);

    expect(mockedNext).toHaveBeenCalledWith(rejectValue);
  });

  it('should call next from middleware when authorization is bypassed', async () => {
    process.env = {
      BYPASS_AUTHORIZATION: true
    };
    const req = {};

    await checkToken(req, mockedRes, mockedNext);

    expect(mockedNext).toHaveBeenCalled();
  });
});
