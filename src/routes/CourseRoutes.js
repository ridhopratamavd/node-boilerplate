import express from 'express';
import CourseController from '../controllers/CourseController';

const {
  add,
  edit,
  fetchAll,
  fetchById,
  remove
} = CourseController;

const router = express.Router();

/*
  Course routes
 */
/**
 * @swagger
 *
 * /api/courses:
 *   get:
 *     tags:
 *     - Courses
 *     summary: 'Get List Of Courses'
 *     description: Create demand request with multiple talent request
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *        description: Get Data Successful
 *   post:
 *     tags:
 *     - Courses
 *     summary: 'Create Course'
 *     description: Create course
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *        description: Create course successful
 * /api/courses/{id}:
 *   get:
 *     tags:
 *     - Courses
 *     summary: 'Get Course Detail'
 *     description: 'Get Course Detail By Course ID'
 *     produces:
 *       - application/json
 *     parameters:
 *     - in: "path"
 *       name: "id"
 *       description: "Course ID"
 *       required: true
 *     responses:
 *       200:
 *        description: Create Successful
 *       404:
 *        description: Resource Not Found
 *   patch:
 *    tags:
 *      - Courses
 *    summary: 'Update Courses data'
 *    description: Update Course Detail By Course ID
 *    produces:
 *      - application/json
 *    parameters:
 *     - in: "path"
 *       name: "id"
 *       description: "Course ID"
 *       required: true
 *    responses:
 *       200:
 *        description: Update Successful
 *       404:
 *        description: Resource Not Found
 *   delete:
 *    tags:
 *      - Courses
 *    summary: 'Delete Courses data'
 *    description: Delete Course Data with Course ID
 *    produces:
 *      - application/json
 *    parameters:
 *    - in: "path"
 *      name: "id"
 *      description: "Course ID"
 *      required: true
 *    responses:
 *      200:
 *       description: Delete Successful
 *      404:
 *       description: Resource Not Found
 */
router.get('/', fetchAll);
router.post('/', add);
router.get('/:id', fetchById);
router.patch('/:id', edit);
router.delete('/:id', remove);

export default router;
