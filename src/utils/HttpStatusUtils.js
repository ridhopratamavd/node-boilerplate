import httpStatusConstants from '../constants/HttpStatusConstants';

const {
  HTTP_STATUS_OK,
  HTTP_STATUS_CREATED,
  HTTP_STATUS_ACCEPTED,
  HTTP_STATUS_BAD_REQUEST,
  HTTP_STATUS_UNAUTHORIZED,
  HTTP_STATUS_FORBIDDEN,
  HTTP_STATUS_NOT_FOUND,
  HTTP_STATUS_CONFLICT,
  HTTP_STATUS_INTERNAL_SERVER_ERROR,
  HTTP_STATUS_SERVICE_UNAVAILABLE
} = httpStatusConstants;

export const isHttpStatusOK = (status) => {
  return status === HTTP_STATUS_OK;
};

export const isHttpStatusAccepted = (status) => {
  return status === HTTP_STATUS_ACCEPTED;
};

export const isHttpStatusNotFound = (status) => {
  return status === HTTP_STATUS_NOT_FOUND;
};

export const isHttpStatusForbidden = (status) => {
  return status === HTTP_STATUS_FORBIDDEN;
};

export const isHttpStatusUnauthorized = (status) => {
  return status === HTTP_STATUS_UNAUTHORIZED;
};

export const isHttpStatusBadRequest = (status) => {
  return status === HTTP_STATUS_BAD_REQUEST;
};

export const isHttpStatusConflict = (status) => {
  return status === HTTP_STATUS_CONFLICT;
};

export const isHttpStatusCreated = (status) => {
  return status === HTTP_STATUS_CREATED;
};

export const isHttpStatusInternalServerError = (status) => {
  return status === HTTP_STATUS_INTERNAL_SERVER_ERROR;
};

export const isHttpStatusServiceUnavailable = (status) => {
  return status === HTTP_STATUS_SERVICE_UNAVAILABLE;
};

export default {
  isHttpStatusOK,
  isHttpStatusCreated,
  isHttpStatusAccepted,
  isHttpStatusNotFound,
  isHttpStatusForbidden,
  isHttpStatusUnauthorized,
  isHttpStatusBadRequest,
  isHttpStatusConflict,
  isHttpStatusInternalServerError,
  isHttpStatusServiceUnavailable
};
