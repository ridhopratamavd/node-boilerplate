1. Add Dockerfile, template.yml and groovy files by generating the scripts through Cakra
2. Change the `nService` name in all the groovy files to not contain upper cases (the pipeline in Cakra will fail if `nService` contains upper case)
